#!/bin/bash

## install master consul

IP=$(hostname -I | awk '{print $2}')
echo "START - install prometheus - "$IP

echo "[1]: install utils and prometheus"
apt-get update -qq >/dev/null
apt-get install -qq -y wget unzip prometheus >/dev/null

echo "[2]: install docker and exporter"
curl -fsSL https://get.docker.com | sh; >/dev/null
docker run -d -p 9107:9107 prom/consul-exporter --consul.server=10.10.12.10:8500

echo "[3]: prometheus conf"
echo "
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
  external_labels:
    monitor: 'codelab-monitor'
rule_files:
scrape_configs:
  - job_name: Consul
    consul_sd_configs:
      - server: '10.10.12.10:8500'
        datacenter: 'mydc'
    relabel_configs:
      - source_labels: [__meta_consul_tags]
        regex: .*,metrics,.*
        action: keep
      #- source_labels: ['__address__']
        #separator:     ':'
        #regex:         '(.*):(.*)'
        #target_label:  '__address__'
        # The first regex result is used for the replacement rule, which
        # is the host address without port. 9100 is appended to the address
        #replacement:   '${1}:8080'
      - source_labels: [__meta_consul_service]
        target_label: job
      - source_labels: [__meta_consul_node]
        target_label: instance
" >/etc/prometheus/prometheus.yml
service prometheus restart
systemctl enable prometheus

echo "[4]: install grafana"

sudo wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt-get update -qq >/dev/null
apt-get install -qq -y grafana >/dev/null
sudo systemctl start grafana-server
sudo systemctl enable grafana-server

sleep 10
curl 'http://localhost:3000/api/datasources' -X POST -H 'Content-Type: application/json;charset=UTF-8' --data-binary '{"name":"Prometheus","type":"prometheus","url":"http://prometheus:9090","access":"proxy","isDefault":true}'


echo "END - install prometheus / grafana"