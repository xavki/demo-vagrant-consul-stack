#!/bin/bash

## install vault

VAULT_VERSION="1.6.1"
IP=$(hostname -I | awk '{print $2}')

echo "START - install vault - "$IP

echo "[1]: install vault"

wget -q https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip
unzip /home/vagrant/vault_${VAULT_VERSION}_linux_amd64.zip
mv /home/vagrant/vault /usr/local/bin/

echo "[2]: autocomplétion"
vault -autocomplete-install
complete -C /usr/local/bin/vault vault

echo "[3] : swap off"
sudo setcap cap_ipc_lock=+ep /usr/local/bin/vault

echo "[4] : création du user vault"
mkdir /etc/vault.d
sudo useradd --system --home /etc/vault.d --shell /bin/false vault

echo "[5] : création du fichier de conf vault.hcl"

echo '
ui = true
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault"
}

listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
}

telemetry {
  statsite_address = "127.0.0.1:8125"
  disable_hostname = true
}'>/etc/vault.d/vault.hcl

echo "[6] : création du service systemd"
echo '[Unit]
Description=Hashicorp Vault
Documentation=https://www.vaultproject.io/docs/
After=network-online.target
Wants=network-online.target
ConditionFileNotEmpty=/etc/vault.d/vault.hcl
StartLimitIntervalSec=60
StartLimitBurst=3

[Service]
User=vault
Group=vault
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=yes
PrivateDevices=yes
SecureBits=keep-caps
AmbientCapabilities=CAP_IPC_LOCK
Capabilities=CAP_IPC_LOCK+ep
CapabilityBoundingSet=CAP_SYSLOG CAP_IPC_LOCK
NoNewPrivileges=yes
ExecStart=/usr/local/bin/vault server -config=/etc/vault.d/vault.hcl
ExecReload=/bin/kill --signal HUP $MAINPID
KillMode=process
KillSignal=SIGINT
Restart=on-failure
RestartSec=5
TimeoutStopSec=30
StartLimitInterval=60
StartLimitIntervalSec=60
StartLimitBurst=3
LimitNOFILE=65536
LimitMEMLOCK=infinity

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/vault.service

echo "[6] : consul start service"
systemctl enable vault
service vault start
sleep 10

echo "[7] : initialise vault"
echo "VAULT_ADDR=http://127.0.0.1:8200">> /etc/environment
sudo vault operator init > vault.keys
sudo chmod 500 vault.keys
export_keys=$(sudo grep "Unseal Key" vault.keys| sed -e "s/Unseal\| //g" | sed s/:/=\"/g | awk '{print "export "$0"\""}')
echo "
#!/bin/bash
# script pour dévérouiller vault

${export_keys}

vault operator unseal \$Key1
vault operator unseal \$Key2
vault operator unseal \$Key3
"> unseal.sh
export_token=$(cat vault.keys | grep "Root Token" | sed s/"I.*: "//g)
echo "VAULT_TOKEN=${export_token}" >>/etc/environment

sudo chmod 500 unseal.sh

echo "END - install vagrant"
